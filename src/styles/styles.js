import { Dimensions, StyleSheet} from 'react-native'

export const stylesSignUp = StyleSheet.create({
  BackGroundImage: {
    flex: 1,
    height: null,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden"
  },
  BackGroundImageSignUp: {
    height: 50,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
  },
  TextSignUp: {
    fontFamily: 'Rubik-Medium',
    color: 'white',
    fontSize: 18
  },
  BackGroundImageContinue: {
    height: 55,
    width: Dimensions.get('window').width - 100,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 120,
    position: 'relative'
  },
  TextContinue: {
    fontFamily: 'Rubik-Medium',
    color: 'white',
    fontSize: 18
  },
  Icon: {
    height: 68,
    width: 68,
    marginBottom: 8
  },
  TextRubikBold:{
    fontSize: 30,
    fontFamily: 'Rubik-Bold'
  },
  TextRubikLight:{
    fontSize: 15,
    fontFamily: 'Rubik-Light'
  },
  TextRubikRegular:{
    fontSize: 17,
    fontFamily: 'Rubik-Regular'
  },
  TextEmailAndPass: {
    color: 'gray',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    textAlign: 'left',
    marginLeft: 64,
    marginBottom: 1
  },
  TextInputStyle: {
    marginBottom: 8,
    marginLeft: 60,
    borderBottomColor: '#b1aeb5',
    borderBottomWidth: 1,
    width: Dimensions.get('window').width - 130,
    fontSize: 15
  },
  ErrorStyle: {
    color: 'red',
    fontFamily: 'Rubik-Bold',
    fontSize: 15,
    margin: 5,
    textAlign: 'center'
  }
 });

 export const stylesLogin = StyleSheet.create({
  TextInputStyle: {
    marginBottom: 10,
    marginLeft: 60,
    borderBottomColor: '#b1aeb5',
    borderBottomWidth: 1,
    width: Dimensions.get('window').width - 130,
    fontSize: 15
  },
  ErrorStyle: {
    color: 'red',
    fontFamily: 'Rubik-Bold',
    fontSize: 15,
    margin: 8,
    textAlign: 'center'
  },
  BackGroundImage: {
    flex: 1,
    height: null,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden"
  },
  Icon: {
    height: 80,
    width: 80,
    marginBottom: 10
  },
  TextRubikBold:{
    fontSize: 30,
    fontFamily: 'Rubik-Bold'
  },
  TextRubikLight:{
    fontSize: 15,
    fontFamily: 'Rubik-Light'
  },
  TextRubikRegular:{
    fontSize: 17,
    fontFamily: 'Rubik-Regular'
  },
  BackGroundImageSignUp: {
    height: 50,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
  },
  TextSignUp: {
    fontFamily: 'Rubik-Medium',
    color: 'white',
    fontSize: 18
  },
  BackGroundImageContinue: {
    height: 55,
    width: Dimensions.get('window').width - 100,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 120,
    position: 'relative'
  },
  TextContinue: {
    fontFamily: 'Rubik-Medium',
    color: 'white',
    fontSize: 18
  },
  ForgotPassword: {
    fontFamily: 'Rubik-Regular',
    color: 'gray',
    fontSize: 16,
    textDecorationLine: 'underline'
  },
  TextEmailAndPass: {
    color: 'gray',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    textAlign: 'left',
    marginLeft: 64,
    marginBottom: 1
  }

 });

 export const stylesHome = StyleSheet.create({
      BackGroundImage: {
        flex: 1,
        height: null,
        width: Dimensions.get('window').width,
        resizeMode: "cover",
        overflow: "hidden",
        padding: 10,
        borderColor: '#000000'
      },
      ViewHome: {
        alignItems: 'center'
      },
      Icon: {
        height: 80,
        width: 80,
        marginBottom: 10
      },
      TextRubikBold:{
        fontSize: 30,
        fontFamily: 'Rubik-Bold'
      },
      TextRubikLight:{
        fontSize: 15,
        fontFamily: 'Rubik-Light',
        textAlign: 'center'
      },
      TextRubikRegular:{
        fontSize: 17,
        fontFamily: 'Rubik-Regular'
      },
      ViewButton:{
        alignItems: 'center',
        justifyContent: 'center'
      },
      ButtonView:{
        paddingBottom: 10, paddingTop: 10,
        paddingLeft: 40, paddingRight: 40,
        marginTop: 5,
        marginBottom: 30,
        borderRadius: 55,
        shadowColor: 'rgba(46, 229, 157, 0.4)',
        shadowOpacity: 1.5,
        elevation: 21,
        shadowRadius: 20 ,
        shadowOffset : { width: 111, height: 13},
        backgroundColor: '#2EE59D'
      },
      ButtonViewLogOut:{
        paddingBottom: 10, paddingTop: 10,
        paddingLeft: 40, paddingRight: 40,
        marginTop: 5,
        marginBottom: 30,
        borderRadius: 55,
        shadowColor: 'rgba(46, 229, 157, 0.4)',
        shadowOpacity: 1.5,
        elevation: 25,
        shadowRadius: 20 ,
        shadowOffset : { width: 111, height: 13},
        backgroundColor: '#f51d45'
      },
      TextViewTouchable:{
        color: 'white',
        fontFamily: 'Rubik-Bold',
        textAlign: 'center'
      },
      Heart: {
        height: 35,
        width: 35
      },
      Heart2: {
        height: 37,
        width: 37
      },
      Heart3: {
        height: 39,
        width: 39
      },
      ImageView: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
      }
 });

 export const stylesGame = StyleSheet.create({
    Container: {
        flex: 1
      },
      AnswerContainer:{
        alignItems:'center',
        justifyContent: 'center',
        textAlign: 'center',
        top: -15
      },
      Title: {
        fontFamily: 'Rubik-Medium',
        fontSize: 17,
        color: 'white',
        textAlign: 'center'
      },
      SubTitle: {
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        fontSize: 21,
        color: 'white',
        margin: 40,
        padding: 12  
      },
      Input:{
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        borderWidth: 2,
        borderColor: '#ffbc00',
        borderRadius: 8,
        fontSize: 17,
        color: 'black',
        margin: 12,
        marginHorizontal: 21 
      },
      Unknown:{
        fontFamily: 'Rubik-Regular',
        borderWidth: 1,
        borderColor: '#cccccc',
        borderRadius: 5,
        margin: 3,
        paddingVertical: 12,
        paddingHorizontal: 70,
        fontSize: 20
      },
      Answer:{
        fontFamily: 'Rubik-Medium',
        borderWidth: 1,
        borderColor: '#00ebcf',
        borderRadius: 5,
        margin: 3,
        paddingVertical: 14,
        paddingHorizontal: 20,
        fontSize: 12
      },
      InfoText:{
        fontSize: 12,
        fontWeight: 'bold',
        marginBottom: 1,
        color: '#9c2d25',
        marginLeft: 6
      },
      Timer:{
        fontFamily: 'Rubik-Regular',
        fontSize: 18,
        fontWeight: '600',
        backgroundColor: 'white',
        color: '#abacaf',
        borderRadius: 999,
        width: 47,
        height: 46,
        borderWidth: 6,
        borderColor: '#ffbc00',
        paddingLeft: 13,
        paddingTop: 13,
        margin: 8
      },
      TimerRed:{
        fontFamily: 'Rubik-Regular',
        fontSize: 17,
        fontWeight: '600',
        backgroundColor: 'white',
        color: '#abacaf',
        borderRadius: 999,
        width: 47,
        height: 46,
        borderWidth: 6,
        borderColor: '#ae2072',
        paddingLeft: 18,
        paddingTop: 13,
        margin: 8
      },
      flexView:{
        flexDirection: 'row',
        justifyContent: 'space-between'
      },
      TextViewTouchable:{
        color: 'white',
        fontWeight: '700',
        textAlign: 'center',
        fontSize: 15
      },
      Images: {
        height: 46,
        width: 46
      },
      ContentTop: {
        flexDirection: 'row',
        justifyContent: 'space-between'
      },
      Gif: {
        height: 40,
        width: 40,
        borderRadius: 100
      },
      BOT: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        margin: 4
      },
      BotName: {
        fontSize: 15,
        fontFamily: 'Rubik-Medium',
        color: '#00674e'
      },
      BotPoint: {
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        backgroundColor: '#a30740',
        color: 'white',
        borderRadius: 999,
        width: 45,
        height: 30,
        textAlign: 'center',
        justifyContent: 'center',
        padding: 7
      },
      Arrow: {
        width: 40,
        height: 40
      },
      TextContinue: {
        fontFamily: 'Rubik-Medium',
        color: 'white',
        fontSize: 18
      },
      BackGroundImageContinue: {
        height: 55,
        width: Dimensions.get('window').width - 100,
        resizeMode: "cover",
        overflow: "hidden",
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 120,
        position: 'relative'
      }
 });

 export const stylesFastMoney = StyleSheet.create({
  Container: {
    flex: 1
  },
  Title: {
    fontFamily: 'Rubik-Medium',
    fontSize: 17,
    color: 'white',
    textAlign: 'center'
  },
  SubTitle: {
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    fontSize: 21,
    color: 'white',
    padding: 12,
    marginRight: 35, marginLeft: 35,
    marginTop: 35, marginBottom: 10  
  },
  Input:{
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
    borderWidth: 2,
    borderColor: '#ffbc00',
    borderRadius: 8,
    fontSize: 17,
    color: 'black',
    margin: 12,
    marginHorizontal: 21    
  },
  Timer:{
    fontFamily: 'Rubik-Regular',
    fontSize: 18,
    fontWeight: '600',
    backgroundColor: 'white',
    color: '#abacaf',
    borderRadius: 999,
    width: 47,
    height: 46,
    borderWidth: 6,
    borderColor: '#ffbc00',
    paddingLeft: 13,
    paddingTop: 13,
    margin: 8
  },
  TimerRed:{
    fontFamily: 'Rubik-Regular',
    fontSize: 17,
    fontWeight: '600',
    backgroundColor: 'white',
    color: '#abacaf',
    borderRadius: 999,
    width: 47,
    height: 46,
    borderWidth: 6,
    borderColor: '#ae2072',
    paddingLeft: 18,
    paddingTop: 13,
    margin: 8
  },
  flexView:{
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  ContentTop: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  BOT: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    margin: 4
  },
  BotName: {
    fontSize: 15,
    fontFamily: 'Rubik-Medium',
    color: '#00674e'
  },
  BotPoint: {
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    backgroundColor: '#a30740',
    color: 'white',
    borderRadius: 999,
    width: 45,
    height: 30,
    textAlign: 'center',
    justifyContent: 'center',
    padding: 7
  },
  Images: {
    height: 46,
    width: 46
  },
  BotContent: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  Gif: {
    height: 40,
    width: 40,
    borderRadius: 100
  },
  BackGroundImageContinue: {
    height: 55,
    width: Dimensions.get('window').width - 100,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 120,
    position: 'relative'
  },
  TextContinue: {
    fontFamily: 'Rubik-Medium',
    color: 'white',
    fontSize: 18
  },

  // The Table of FastMoney Screen
  tableView: {
    marginLeft: '10%',
    top: '-2%'
  },
  tableBorderStyle: { 
    borderWidth: 12,
    borderColor: 'white'
  },
  wrapper: {
    flexDirection: 'row',
    overflow: 'hidden'
  },
  row: {
    height: 45,
    width: Dimensions.get('window').width - 81,
    backgroundColor: '#12294d'
  },
  text: {
    textAlign: 'center',
    fontSize: 21,
    fontFamily: 'Rubik-Medium',
    color: 'white'
  }
});

export const stylesMatchScreen = StyleSheet.create({
  Container: {
    flex: 1,
    borderColor: '#000000',
    borderWidth: 25,
    backgroundColor: '#bfd8db',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  Images: {
    height: 180,
    width: 180
  },
  botName: {
    fontFamily: 'Rubik-Medium',
    fontSize: 36,
    marginBottom: 12
  }
});

export const stylesSplash = StyleSheet.create({
  BackGroundImage: {
    flex: 1,
    height: null,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center'
  },
  Icon: {
    height: 90,
    width: 90
  }
});

export const stylesForgotPW = StyleSheet.create({
  BackGroundImage: {
    flex: 1,
    height: null,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden"
  },
  BackGroundImageSignUp: {
    height: 50,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
  },
  TextSignUp: {
    fontFamily: 'Rubik-Medium',
    color: 'white',
    fontSize: 18
  },
  BackGroundImageContinue: {
    height: 55,
    width: Dimensions.get('window').width - 100,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 120,
    position: 'relative'
  },
  TextContinue: {
    fontFamily: 'Rubik-Medium',
    color: 'white',
    fontSize: 18
  },
  Icon: {
    height: 80,
    width: 80,
    marginBottom: 10
  },
  TextRubikBold:{
    fontSize: 30,
    fontFamily: 'Rubik-Bold'
  },
  TextRubikLight:{
    fontSize: 15,
    fontFamily: 'Rubik-Light'
  },
  TextRubikRegular:{
    fontSize: 17,
    fontFamily: 'Rubik-Regular'
  },
  TextEmailAndPass: {
    color: 'gray',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    textAlign: 'left',
    marginLeft: 64,
    marginBottom: 1
  },
  TextInputStyle: {
    marginBottom: 40,
    marginLeft: 60,
    borderBottomColor: '#b1aeb5',
    borderBottomWidth: 1,
    width: Dimensions.get('window').width - 130,
    fontSize: 15
  }
});

export const stylesLeaderBoard = StyleSheet.create({
  BackGroundImage: {
    flex: 1,
    height: null,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden",
    paddingTop: 60  
  },
  TextUsername: {
    fontSize: 21,
    fontFamily: 'Rubik-Medium',
    color: '#2e313c',
    top: -24,
    left: -4
  },
  TextPoint: {
    fontSize: 18,
    fontFamily: 'Rubik-Light',
    color: '#abacaf',
    top: -22,
    textAlign: 'center'
  },
  TextRanking: {
    backgroundColor: '#535353',
    left: 21,
    top: -31,
    color: '#ffffff',
    fontFamily: 'Rubik-Bold',
    fontSize: 16,
    borderRadius: 999,
    padding: 5,
    width: 30,
    textAlign: 'center'
  },
  ViewRender: {
    marginLeft: 12,
    marginRight: 12,
    marginTop: 14,
    alignContent: 'stretch',
    alignItems: 'center'
  },
  Image1: {
    height: 90,
    width: 90
  },
  ViewUser: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '38%'
  },
  Image2: {
    height: 110,
    width: 110,
    borderWidth: 5,
    borderColor: 'white',
    borderRadius: 999
  },
  UserRanking: {
    backgroundColor: '#ffbb00',
    left: 32,
    top: -35,
    color: '#ae5d36', 
    fontFamily: 'Rubik-Bold',
    borderRadius: 999, 
    fontSize: 20, 
    paddingHorizontal: 13, 
    paddingVertical: 5
  },
  ViewUser2: {
    top: -20,
    left: 4
  },
  currentUsername: {
    fontSize: 34,
    fontFamily: 'Rubik-Medium',
    color: 'white'
  },
  currentPoint: {
    fontSize:21,
    fontFamily: 'Rubik-Light',
    color: '#ffffff',
    textAlign: 'center'
  },
  ViewFlatlist: {
    alignItems:'center',
    justifyContent: 'center'
  }
});

export const stylesCongrat = StyleSheet.create({
  BackGroundImage: {
    flex: 1,
    height: null,
    width: Dimensions.get('window').width,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center'
  },
  Icon: {
    borderWidth: 5,
    borderColor: 'white',
    borderRadius: 999,
    height: 150,
    width: 150
  },
  Icon2: {
    height: 70,
    width: 70
  },
  Icon3: {
    height: 70,
    width: 70
  },
  TextStyle: {
    color: 'white',
    fontFamily: 'Rubik-Medium',
    fontSize: 47,
    textAlign: 'center',
    margin: 12
  },
  TextStyle2: {
    color: 'white',
    fontFamily: 'Rubik-Regular',
    fontSize: 25,
    textAlign: 'center'
  },
  BackGroundImageContinue: {
    height: 62,
    width: Dimensions.get('window').width - 100,
    resizeMode: "cover",
    overflow: "hidden",
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 120,
    position: 'relative'
  },
  TextContinue: {
    fontFamily: 'Rubik-Medium',
    color: 'white',
    fontSize: 18
  },
  mainView: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-evenly'
  }
});

export const stylesLoginScreen = StyleSheet.create({
  ImageBackgroundStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    fontFamily: 'Rubik-Medium',
    fontSize: 24,
    color:'black'
  }
});







 