import firebase from 'firebase';

if (!firebase.apps.length) {
    firebase.initializeApp({
    apiKey: "AIzaSyAIzMentYafAL2kNtf2jSQqyE5KuQCmeag",
    authDomain: "asked100people.firebaseapp.com",
    databaseURL: "https://asked100people.firebaseio.com",
    projectId: "asked100people",
    storageBucket: "asked100people.appspot.com",
    messagingSenderId: "718970675451",
    appId: "1:718970675451:web:5453d1bb655dde9fe1d05e",
    measurementId: "G-8PZNS9F0T8"
  });
}

firebase.firestore().settings({ experimentalForceLongPolling: true });
export const db = firebase.firestore();