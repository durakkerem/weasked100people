import React, {Component} from 'react'
import { AppState ,View, Text, TouchableOpacity, Image, Alert, ImageBackground, StatusBar } from 'react-native'
import firebase from 'firebase';
import {db} from '../../config';
import { AdMobRewarded } from 'react-native-admob'
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import I18n from 'react-native-i18n';
import StringsOfLanguages from '../../StringsOfLanguages';
import { stylesHome, stylesLogin } from '../../styles/styles'

export default class HomePage extends Component {

    constructor(props){
      super(props);

      this.state = {
        appState: AppState.currentState,
        questions: {},
        Right: null,
        lastRightTime: ''
      };
    }

    tempQuestions = [];
    randomDataId = [];

    fetchQuestions(maxVal) {
      var currentLanguage = I18n.currentLocale(); // Get the Current Language from Device
      var min = 1;
      var max = maxVal;
      this.randomDataId = [];
      this.tempQuestions = [];
  
      for(var i = 0; i < 8; i++){
        this.randomDataId.push('' + Math.floor(min + (Math.random() * (max - min))));
      }

      if(currentLanguage === "tr-TR" || currentLanguage === "tr-CY")
      {
          db.collection("questionsTr")
          .where(firebase.firestore.FieldPath.documentId(), 'in', this.randomDataId).get()
            .then((querySnapshot) => {
              querySnapshot.forEach((doc) => {
                this.tempQuestions.push(doc.data());
            });
          }).then(() => {
            this.tempQuestions.push({question: ""});
          });
      }
      else
      {
          db.collection("questionsEng")
          .where(firebase.firestore.FieldPath.documentId(), 'in', this.randomDataId).get()
            .then((querySnapshot) => {
              querySnapshot.forEach((doc) => {
                this.tempQuestions.push(doc.data());
            });
          }).then(() => {
            this.tempQuestions.push({question: ""});
          });
      }

    }// end of the function
    

    initiliazeUserRight(){
      AsyncStorage.getItem('Right').then((userRight) => {

        if(userRight === null){
          this.setState({Right: '3'}, () => {
            AsyncStorage.setItem('Right', '3');
          });
        }
        
        else{

          if(userRight === '3')  
          {
            this.setState({Right: userRight});
          }
          else
          {
            this.setState({Right: userRight});

            AsyncStorage.getItem('lastRightTime').then((userLastRightTime) => {
            
              if(userLastRightTime === null){
                var date = moment()
                  .utcOffset('+03:00')
                  .format('YYYY-MM-DD HH:mm:ss a');
  
                var lastRightTime = date[11]+date[12]+" "+date[14]+date[15];
              
                AsyncStorage.setItem('lastRightTime', lastRightTime);
              }
              else{
                var date1 = moment()
                .utcOffset('+03:00')
                .format('YYYY-MM-DD HH:mm:ss a');

                var date1Hour = parseInt(date1[11]+date1[12]);   // Current Hour
                var date1Minute = parseInt(date1[14]+date1[15]); // Current Minute

                if(date1Hour === parseInt(userLastRightTime[0]+userLastRightTime[1]))
                {

                  var result = date1Minute - parseInt(userLastRightTime[3]+userLastRightTime[4]);

                  if(result > 5)
                  {
                    switch (userRight) {
                      case '2':
                        this.setState({Right: '3'}, () => {
                          AsyncStorage.setItem('Right', '3');
                        });
                        break;

                      case '1':
                        this.setState({Right: '2'}, () => {
                          AsyncStorage.setItem('Right', '2');
                        });
                        break;

                      case '0':
                        this.setState({Right: '1'}, () => {
                          AsyncStorage.setItem('Right', '1');
                        });
                        break;
                    
                      default:
                        null;
                        break;
                    }
                  }
                  else if(result > 10)
                  {
                    switch (userRight) {
                      case '2':
                        this.setState({Right: '3'}, () => {
                          AsyncStorage.setItem('Right', '3');
                        });
                        break;

                      case '1':
                        this.setState({Right: '3'}, () => {
                          AsyncStorage.setItem('Right', '3');
                        });
                        break;

                      case '0':
                        this.setState({Right: '2'}, () => {
                          AsyncStorage.setItem('Right', '2');
                        });
                        break;
                    
                      default:
                        null;
                        break;
                    }
                  }
                  else if(result > 15)
                  {
                    this.setState({Right: '3'}, () => {
                      AsyncStorage.setItem('Right', '3');
                    });
                  }

                }

                else
                {

                  // en az 1 saat fark olacağı için canları yenile

                  this.setState({Right: '3'}, () => {
                    AsyncStorage.setItem('Right', '3');
                  });

                }
              } // end of the main else
  
            });
          }
        }

      });

    }// end of the fucntion

    goToGameScreen = () => {

      var date3 = moment()
      .utcOffset('+03:00')
      .format('YYYY-MM-DD HH:mm:ss a');

      var lastRightTime4 = date3[11]+date3[12]+" "+date3[14]+date3[15];
    
      AsyncStorage.setItem('lastRightTime', lastRightTime4);

      var currentRight = parseInt(this.state.Right);

      if (currentRight > 0) {
        currentRight = currentRight - 1;
        this.setState({Right: currentRight.toString()}, () => {
          AsyncStorage.setItem('Right', currentRight.toString());
          this.props.navigationObject.navigate('Match', {questions: this.tempQuestions});
        });
      } 

      else {
        Alert.alert(StringsOfLanguages.finishRight);
      }
    }

    // Why i write this function -> Because, after the finish game and the screen is going to HomePage, the questions doesn't reload.
    // With this function, after the finish game, the questions going to be reload.
    _handleAppStateChange = (nextAppState) => {
      if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
        // Actual question counts
        var currentLanguage = I18n.currentLocale(); // Get the Current Language from Device
        if(currentLanguage === "tr-TR" || currentLanguage === "tr-CY")
        {
          var maxVal2 = 0;
          db.collection("questionCounts").where(firebase.firestore.FieldPath.documentId(), 'in', ['tr']).get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                  maxVal2 = doc.data().count;
            });                                                   
          }).then(() => this.fetchQuestions(maxVal2));
        }
        else
        {
          var maxVal3 = 0;
          db.collection("questionCounts").where(firebase.firestore.FieldPath.documentId(), 'in', ['eng']).get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                  maxVal3 = doc.data().count;
            });                                                 
          }).then(() => this.fetchQuestions(maxVal3));
        }
        // -
      }
      this.setState({appState: nextAppState});
    }

    showRewarded() {
      AdMobRewarded.showAd().catch(() => Alert.alert(StringsOfLanguages.notReady));
    }

    showRight() {
      switch (this.state.Right) {
        case '3':
          return  <View style={stylesHome.ImageView}>
            <Image 
              source={require('../../../images/heart.png')}
              style={stylesHome.Heart}/>
            <Image 
              source={require('../../../images/heart.png')}
              style={stylesHome.Heart}
            />
            <Image 
              source={require('../../../images/heart.png')}
              style={stylesHome.Heart}
            />
        </View>
          break;
        case '2':
          return  <View style={stylesHome.ImageView}>
          <Image 
            source={require('../../../images/heart.png')}
            style={stylesHome.Heart2}/>
          <Image 
            source={require('../../../images/heart.png')}
            style={stylesHome.Heart2}
          />
        </View>
          break;
        case '1':
          return  <View style={stylesHome.ImageView}>
          <Image 
            source={require('../../../images/heart.png')}
            style={stylesHome.Heart3}
          />
        </View>
          break;
      
        default:
          return  <View style={stylesHome.ImageView}>
        </View>
          break;
      }
    }

    render() {
      return (
        <ImageBackground source={require('../../../images/background.png')} style={stylesHome.BackGroundImage}>   

          <View>
            {this.showRight()}
          </View>

          <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-evenly', top: -25}}> 
            <View style={stylesHome.ViewHome}>
              <Image 
                  source={require('../../../images/logo.png')}
                  style={stylesHome.Icon}
              />
              <Text style={stylesHome.TextRubikBold}>{StringsOfLanguages.mainTitle}</Text>
              <Text style={stylesHome.TextRubikLight}>{StringsOfLanguages.messageOfHomepage}</Text>
            </View>

            <View style={stylesHome.ViewButton}>
                <TouchableOpacity style={stylesHome.ButtonView} onPress={this.goToGameScreen}>
                  <Text style={stylesHome.TextViewTouchable}>{StringsOfLanguages.startGame}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={stylesHome.ButtonView} onPress = { () => this.showRewarded() } >
                  <Text style={stylesHome.TextViewTouchable}>{StringsOfLanguages.watchAd}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={stylesHome.ButtonView} onPress={() => this.props.navigationObject.navigate('LeaderBoard')} >
                  <Text style={stylesHome.TextViewTouchable}>{StringsOfLanguages.leaderBoard}</Text>
                </TouchableOpacity>

                <TouchableOpacity style={stylesHome.ButtonViewLogOut} onPress={() => firebase.auth().signOut()} >
                  <Text style={stylesHome.TextViewTouchable}>{StringsOfLanguages.logout}</Text>
                </TouchableOpacity>
            </View>
          </View>

        </ImageBackground>
          
      )
    }

    componentDidMount() {
      StatusBar.setHidden(true);
      AppState.addEventListener('change', this._handleAppStateChange);

      //#region GET QUESTIONS
      var currentLanguage = I18n.currentLocale(); // Get the Current Language from Device
      if(currentLanguage === "tr-TR" || currentLanguage === "tr-CY")
      {
        var maxVal = 0;
        db.collection("questionCounts").where(firebase.firestore.FieldPath.documentId(), 'in', ['tr']).get()
          .then((querySnapshot) => {
              querySnapshot.forEach((doc) => {
                maxVal = doc.data().count;
          });                                                   // RİGHT - ASYNC STORAGE
        }).then(() => this.fetchQuestions(maxVal)).then(() => {this.initiliazeUserRight()});
      }
      else
      {
        var maxVal1 = 0;
        db.collection("questionCounts").where(firebase.firestore.FieldPath.documentId(), 'in', ['eng']).get()
          .then((querySnapshot) => {
              querySnapshot.forEach((doc) => {
                maxVal1 = doc.data().count;
          });                                                   // RİGHT - ASYNC STORAGE
        }).then(() => this.fetchQuestions(maxVal1)).then(() => {this.initiliazeUserRight()});
      }
      //#endregion


      //#region AD
      AdMobRewarded.setAdUnitID('ca-app-pub-3940256099942544/5224354917');
      AdMobRewarded.addEventListener('rewarded', () => {
        this.setState({Right: '3'}, () => {
          AsyncStorage.setItem('Right', '3');
        });
      });
      AdMobRewarded.addEventListener('adFailedToLoad', () => {
        console.log("Failed to Loading Ad");
      });
      AdMobRewarded.addEventListener('adClosed', () => {
        AdMobRewarded.requestAd().catch(() => null);
        // if there is no catch function, when start the game , coming the warning on the screen.
      });
      AdMobRewarded.requestAd().catch(() => null);
      //#endregion
    }

    componentWillUnmount() {
      AppState.removeEventListener('change', this._handleAppStateChange);
      AdMobRewarded.removeAllListeners();
    }

}




