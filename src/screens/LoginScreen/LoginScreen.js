import React, { Component } from 'react'
import { Text, ImageBackground } from 'react-native'
import LoginForm from './LoginForm'
import HomePage from './HomePage'
import firebase from 'firebase'
import { stylesLoginScreen } from '../../styles/styles'
import StringsOfLanguages from '../../StringsOfLanguages';


export default class LoginScreen extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            isLoggedIn: null
        };
      }

    UNSAFE_componentWillMount() {
        firebase.auth().onAuthStateChanged((user) => {
        if(user) {
            this.setState({ isLoggedIn: true });
        }
        else {
            this.setState({ isLoggedIn: false });
        }
        });
    }

    sayLoading() {
    return  <ImageBackground 
                source={require('../../../images/background.png')} 
                style={stylesLoginScreen.ImageBackgroundStyle} >
                <Text style={stylesLoginScreen.textStyle}>{StringsOfLanguages.loading}</Text>
            </ImageBackground>;
    }

    renderContent() {
        switch(this.state.isLoggedIn) {
        case true:
            return <HomePage navigationObject={this.props.navigation} />;
        case false:
            return <LoginForm navigationObject={this.props.navigation} />;
        default:
            return this.sayLoading();
        }
    }


    render() {
        return (
            this.renderContent()
        )
    }

}






