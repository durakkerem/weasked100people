import React, { Component } from 'react'
import { Text, View, ImageBackground, TouchableWithoutFeedback, Keyboard, TextInput, TouchableOpacity, Image, StatusBar } from 'react-native'
import firebase from 'firebase'
import StringsOfLanguages from '../../StringsOfLanguages';
import { stylesLogin } from '../../styles/styles'

export default class LoginForm extends Component {

    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: '',
            error: '',
            loading: false
        };
      }

      onLoginSucsess() {
        Keyboard.dismiss();
      }
      
      onLoginFail() {
        Keyboard.dismiss();
        this.setState({ error: StringsOfLanguages.errorLogin, loading: false});
      }


      
    submitHandler() {
        const {email, password} = this.state;
        this.setState({ error: '', loading: true});

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(this.onLoginSucsess.bind(this))
            .catch(this.onLoginFail.bind(this));
    }

    render() {
        return (
        <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
            <ImageBackground 
                source={require('../../../images/background.png')} 
                style={stylesLogin.BackGroundImage} 
            >
                <View style={{alignItems: 'center', marginTop: 80, marginBottom:45}}>
                    <Image 
                        source={require('../../../images/logo.png')}
                        style={stylesLogin.Icon}
                    />
                    <Text style={stylesLogin.TextRubikBold}>{StringsOfLanguages.mainTitle}</Text>
                    <Text style={stylesLogin.TextRubikLight}>{StringsOfLanguages.underTitleLogin}</Text>
                </View>

                <View>

                    <Text style={stylesLogin.TextEmailAndPass}>Email</Text>
                    <View>
                        <TextInput
                            style = {stylesLogin.TextInputStyle}
                            value = {this.state.email}
                            onChangeText = {email => this.setState({ email })}
                            placeholder = {StringsOfLanguages.placeholderLogin}
                            autoCompleteType = 'email'
                            returnKeyType = 'send'                  
                        />
                    </View>

                    <Text style={stylesLogin.TextEmailAndPass}>{StringsOfLanguages.password}</Text>
                    <View>
                        <TextInput
                            style = {stylesLogin.TextInputStyle}     
                            value = {this.state.password}
                            onChangeText = {password => this.setState({ password })}     
                            secureTextEntry={true}         
                            placeholder = '***********'
                            autoCompleteType = 'password' 
                            returnKeyType = 'send'                  
                        />
                    </View>


                    <View style={{alignItems: 'flex-end', marginRight: 70,marginTop: 5}}>
                        <TouchableOpacity 
                            onPress={() => this.props.navigationObject.navigate('ForgotPW')}
                        >
                            <Text style={stylesLogin.ForgotPassword}>{StringsOfLanguages.forgotPW}</Text>
                        </TouchableOpacity>
                    </View>

                </View>

                
                <Text style={stylesLogin.ErrorStyle}>{this.state.error}</Text>

                <View style={{flex: 1, alignItems: 'center'}}>
                    <TouchableOpacity 
                        onPress = {() => this.submitHandler()}
                    >
                        <ImageBackground 
                            source={require('../../../images/continue_button.png')}
                            style={stylesLogin.BackGroundImageContinue}
                        >
                            <Text style={stylesLogin.TextContinue}>{StringsOfLanguages.continue}</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>

                <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end'}}>
                    <TouchableOpacity 
                        onPress = {() => this.props.navigationObject.navigate('Signup')}
                    >
                        <ImageBackground 
                            source={require('../../../images/signup_button.png')}
                            style={stylesLogin.BackGroundImageSignUp}
                        >
                            <Text style={stylesLogin.TextSignUp}>{StringsOfLanguages.signUp}</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>

            </ImageBackground> 
        </TouchableWithoutFeedback>
        )
    }
    componentDidMount() {
        StatusBar.setHidden(true);
    }
}
