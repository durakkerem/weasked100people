import React, { Component } from 'react'
import { Text, View, Alert, ImageBackground, TouchableWithoutFeedback, Keyboard, TextInput, TouchableOpacity, Image, StatusBar } from 'react-native'
import firebase from 'firebase';
import {db} from '../../config';
import StringsOfLanguages from '../../StringsOfLanguages';
import { stylesSignUp } from '../../styles/styles'


export default class SignupForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            username: '',
            email: '',
            password1: '',
            password2: '',
            error: ''
        };
      }

      onLoginSucsess() {
        Keyboard.dismiss();
        Alert.alert(StringsOfLanguages.successSignup);
        this.props.navigationObject.navigate('Login')
      }
      
      onLoginFail() {
        Keyboard.dismiss();
        this.setState({ error: StringsOfLanguages.errorSignup});
      }

      submitHandler() {
        var isUsernameUnique = true;
        this.setState({ error: ''});
        const { email, password1, password2, username } = this.state;

        db.collection("usersCollection").get().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                if(doc.data().username === username)
                {
                    isUsernameUnique = false;
                }
            });
        }).then(() => {
            if (isUsernameUnique === false){
                Alert.alert(StringsOfLanguages.errorSignup2);
            }
            else {
    
                if(password1 === password2) {
                    if (password1.length > 7) {
                        firebase.auth().createUserWithEmailAndPassword(email, password1)
                        .then(registeredUser => {
                            db.collection("usersCollection").doc(username).set({
                                userId: registeredUser.user.uid,
                                email: registeredUser.user.email,
                                username: username,
                                totalPoint: 0
                            })
                        })
                        .then(this.onLoginSucsess.bind(this))
                        .catch(this.onLoginFail.bind(this));
        
                    }
        
                    else {
                        Alert.alert(StringsOfLanguages.errorSignup3);
                    }
                    
                }
                else {
                    this.setState({ error: StringsOfLanguages.errorSignup4});
                }
            }
        });
    } // end of the SubmitHandler() function.


    render() {
        return (
        <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
            <ImageBackground 
                source={require('../../../images/background.png')} 
                style={stylesSignUp.BackGroundImage} 
            >
                <View style={{alignItems: 'center', marginTop: 40, marginBottom:20}}>
                    <Image 
                        source={require('../../../images/logo.png')}
                        style={stylesSignUp.Icon}
                    />
                    <Text style={stylesSignUp.TextRubikBold}>{StringsOfLanguages.mainTitle}</Text>
                    <Text style={stylesSignUp.TextRubikLight}>{StringsOfLanguages.signUp}</Text>
                </View>
                
                <View>
                    <Text style={stylesSignUp.TextEmailAndPass}>{StringsOfLanguages.username}</Text>
                    <View>
                        <TextInput
                            style = {stylesSignUp.TextInputStyle}
                            value = {this.state.username}
                            onChangeText = {username => this.setState({ username })}
                            placeholder = {StringsOfLanguages.placeholderSignup}
                            returnKeyType = 'send'                  
                        />
                    </View>

                    <Text style={stylesSignUp.TextEmailAndPass}>Email</Text>
                    <View>
                        <TextInput
                            style = {stylesSignUp.TextInputStyle}     
                            value = {this.state.email}
                            onChangeText = {email => this.setState({ email })}       
                            placeholder = {StringsOfLanguages.placeholderLogin}
                            autoCompleteType = 'email'
                            returnKeyType = 'send'                  
                        />
                    </View>

                    <Text style={stylesSignUp.TextEmailAndPass}>{StringsOfLanguages.password}</Text>
                    <View>
                        <TextInput
                            style = {stylesSignUp.TextInputStyle}     
                            value = {this.state.password1}
                            onChangeText = {password1 => this.setState({ password1 })} 
                            placeholder = '***********'
                            autoCompleteType = 'password'
                            secureTextEntry={true}  
                            returnKeyType = 'send'                  
                        />
                    </View>

                    <Text style={stylesSignUp.TextEmailAndPass}>{StringsOfLanguages.password2}</Text>
                    <View>
                        <TextInput
                            style = {stylesSignUp.TextInputStyle}     
                            value = {this.state.password2}
                            onChangeText = {password2 => this.setState({ password2 })}       
                            placeholder = '***********'
                            autoCompleteType = 'password'
                            secureTextEntry={true}  
                            returnKeyType = 'send'                  
                        />
                    </View>
                </View>
                
                <Text style={stylesSignUp.ErrorStyle}>{this.state.error}</Text>

                <View style={{flex: 1, alignItems: 'center'}}>
                    <TouchableOpacity 
                        onPress = {() => this.submitHandler()}
                    >
                        <ImageBackground 
                            source={require('../../../images/continue_button.png')}
                            style={stylesSignUp.BackGroundImageContinue}
                        >
                            <Text style={stylesSignUp.TextContinue}>{StringsOfLanguages.continue}</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>

                <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end'}}>
                    <TouchableOpacity 
                        onPress = {() => this.props.navigationObject.navigate('Login')}
                    >
                        <ImageBackground 
                            source={require('../../../images/signup_button.png')}
                            style={stylesSignUp.BackGroundImageSignUp}
                        >
                            <Text style={stylesSignUp.TextSignUp}>{StringsOfLanguages.login}</Text>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>


            </ImageBackground>
        </TouchableWithoutFeedback>
        )
    }
    componentDidMount() {
        StatusBar.setHidden(true);
    }
}
