import React, { Component } from 'react'
import SignupForm from './SignupForm'

export default class SignupScreen extends Component {
    render() {
        return (
            <SignupForm navigationObject={this.props.navigation} />
        )
    }
}
