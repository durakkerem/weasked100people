import React, { Component } from 'react'
import { Text, View, TextInput, Image, TouchableOpacity, ImageBackground, Keyboard, Alert, TouchableWithoutFeedback, StatusBar } from 'react-native'
import firebase from 'firebase';
import StringsOfLanguages from '../../StringsOfLanguages';
import { stylesForgotPW } from '../../styles/styles'

export default class ForgotPassword extends Component {

    constructor(props){
        super(props);
    
        this.state = {
            Email: ''
        };
      }

      onLoginSucsess() {
        Keyboard.dismiss();
        Alert.alert(StringsOfLanguages.pwAlert1);
        this.props.navigationObject.navigate('Login')
      }
      
      onLoginFail() {
        Keyboard.dismiss();
        Alert.alert(StringsOfLanguages.pwAlert2);
      }

    SendEmailToResetPassword()
    {
        const { Email } = this.state;

        firebase.auth().sendPasswordResetEmail(Email)
            .then(this.onLoginSucsess.bind(this))
            .catch(this.onLoginFail.bind(this));

    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
                <ImageBackground 
                    source={require('../../../images/background.png')} 
                    style={stylesForgotPW.BackGroundImage} 
                >

                    <View style={{alignItems: 'center', marginTop: 80, marginBottom:45}}>
                        <Image 
                            source={require('../../../images/logo.png')}
                            style={stylesForgotPW.Icon}
                        />
                        <Text style={stylesForgotPW.TextRubikBold}>{StringsOfLanguages.mainTitle}</Text>
                        <Text style={stylesForgotPW.TextRubikLight}>{StringsOfLanguages.forgotPW2}</Text>
                    </View>

                    <Text style={stylesForgotPW.TextEmailAndPass}>Email</Text>
                    <View>
                        <TextInput
                            style = {stylesForgotPW.TextInputStyle}
                            value = {this.state.Email}
                            onChangeText = {Email => this.setState({ Email })} 
                            placeholder = {StringsOfLanguages.placeholderLogin}
                            autoCompleteType = 'email'
                            returnKeyType = 'send'                  
                        />
                    </View>

                    <View style={{flex: 1, alignItems: 'center'}}>
                        <TouchableOpacity 
                            onPress = {() => this.SendEmailToResetPassword()}
                        >
                            <ImageBackground 
                                source={require('../../../images/continue_button.png')}
                                style={stylesForgotPW.BackGroundImageContinue}
                            >
                                <Text style={stylesForgotPW.TextContinue}>{StringsOfLanguages.continue}</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>

                    <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end'}}>
                        <TouchableOpacity 
                            onPress = {() => this.props.navigation.navigate('Login')}
                        >
                            <ImageBackground 
                                source={require('../../../images/signup_button.png')}
                                style={stylesForgotPW.BackGroundImageSignUp}
                            >
                                <Text style={stylesForgotPW.TextSignUp}>{StringsOfLanguages.login}</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>

                </ImageBackground>
            </TouchableWithoutFeedback>
        )
    }
    componentDidMount() {
        StatusBar.setHidden(true);
    }
}
