import React, { Component } from 'react'
import { Image, ImageBackground, View, StatusBar } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation';
import I18n from 'react-native-i18n';
import StringsOfLanguages from '../../StringsOfLanguages';
import { stylesSplash } from '../../styles/styles'

export default class SplashScreen extends Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <ImageBackground source={require('../../../images/background.png')} style={stylesSplash.BackGroundImage}>
                <View style={{opacity: 0.85}}>
                    <Image 
                        source={require('../../../images/logo.png')}
                        style={stylesSplash.Icon}
                    />
                </View>
            </ImageBackground>
        )
    }

    componentDidMount() {
        this.props.navigation.dispatch(resetAction);
        StatusBar.setHidden(true);
        var currentLanguage = I18n.currentLocale(); // Get the Current Language from Device

        if(currentLanguage === "tr-TR" || currentLanguage === "tr-CY")
        {
          StringsOfLanguages.setLanguage('tr');
          setTimeout(() => { this.props.navigation.navigate('Login') }, 1600);
        }
        else
        {
          StringsOfLanguages.setLanguage('en');
          setTimeout(() => { this.props.navigation.navigate('Login') }, 1600);
        }
      
    }

}// end of the SplashScreen class

const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Login' })],
});
