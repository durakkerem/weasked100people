import React, { Component } from 'react'
import { Text, View, FlatList, Image, ImageBackground, StatusBar } from 'react-native'
import firebase from 'firebase';
import {db} from '../../config';
import StringsOfLanguages from '../../StringsOfLanguages';
import { stylesLeaderBoard } from '../../styles/styles'


export default class LeaderBoard extends Component {


    constructor(props){
        super(props);

        this.state = {
            currentUserUsername: '',
            currentUserTotalPoint: 0,
            userArray: [],
            userRank: '  '
        }
    }

    GetCurrentUserRank() {
        var counter = 0;
        var userRankLocale = '';
        
        var currentUserId = firebase.auth().currentUser.uid;
        db.collection("usersCollection").orderBy('totalPoint','desc').get().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                counter += 1;
                if(doc.data().userId === currentUserId)
                {
                    userRankLocale = counter.toString(); // 1
                }
            });
        }).then(() => {this.setState({userRank: userRankLocale});});
    }
    
    
    SetCurrentUserPoint() {
        const currentUserID = firebase.auth().currentUser.uid;
        var tempCurrentUserTotalPoint = this.state.currentUserTotalPoint;
        var tempCurrentUserUsername = this.state.currentUserUsername;

        db.collection("usersCollection").get().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                if(doc.data().userId === currentUserID)
                {
                    tempCurrentUserTotalPoint = doc.data().totalPoint;
                    tempCurrentUserUsername = doc.data().username;
                }
            });
        }).then(() => this.setState({
            currentUserTotalPoint: tempCurrentUserTotalPoint,
            currentUserUsername: tempCurrentUserUsername
        }));
    }

    SetTheSixHighest(){
        var tempUserArray = this.state.userArray;
        var counter = 0;
        var image1 = require('../../../images/bot1.png')
        var image2 = require('../../../images/bot2.png')
        var image3 = require('../../../images/bot3.png')
        var image4 = require('../../../images/bot4.png')
        var image5 = require('../../../images/bot5.png')
        var image6 = require('../../../images/bot6.png')


        db.collection("usersCollection").orderBy('totalPoint','desc').get().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
                counter += 1;
                if(counter <= 6){
                    switch (counter) {
                        case 1:
                            tempUserArray.push(
                                {
                                    ID: counter,
                                    Username: doc.data().username,
                                    TotalPoint: doc.data().totalPoint,
                                    Image: image1,
                                    Ranking: counter
                                }
                            );
                            break;


                        case 2:
                            tempUserArray.push(
                                {
                                    ID: counter,
                                    Username: doc.data().username,
                                    TotalPoint: doc.data().totalPoint,
                                    Image: image2,
                                    Ranking: counter
                                }
                            );
                            break;

                        case 3:
                            tempUserArray.push(
                                {
                                    ID: counter,
                                    Username: doc.data().username,
                                    TotalPoint: doc.data().totalPoint,
                                    Image: image3,
                                    Ranking: counter
                                }
                            );
                            break;
                        
                        case 4:
                            tempUserArray.push(
                                {
                                    ID: counter,
                                    Username: doc.data().username,
                                    TotalPoint: doc.data().totalPoint,
                                    Image: image4,
                                    Ranking: counter
                                }
                            );
                            break;

                        case 5:
                            tempUserArray.push(
                                {
                                    ID: counter,
                                    Username: doc.data().username,
                                    TotalPoint: doc.data().totalPoint,
                                    Image: image5,
                                    Ranking: counter
                                }
                            );
                            break;

                        case 6:
                            tempUserArray.push(
                                {
                                    ID: counter,
                                    Username: doc.data().username,
                                    TotalPoint: doc.data().totalPoint,
                                    Image: image6,
                                    Ranking: counter
                                }
                            );
                            break;
                    
                        default:
                            null;
                            break;
                    }
                }
            });
        }).then(() => this.setState({
            userArray: tempUserArray
        }));
    }

    _renderMyList = ({item})=>(
        <View style={stylesLeaderBoard.ViewRender}>
            <Image 
                source={item.Image}
                style={stylesLeaderBoard.Image1}
            />
            <Text style={stylesLeaderBoard.TextRanking}>{item.Ranking}</Text>
            <View>
                <Text style={stylesLeaderBoard.TextUsername}>{ item.Username }</Text>
                <Text style={stylesLeaderBoard.TextPoint}>{ item.TotalPoint }</Text>
            </View>
        </View> 
    )

    _renderMyKeyExtractor=(item)=>item.ID.toString();


    render() {
        return (
            <ImageBackground 
                source={require('../../../images/Leaderboard-Light.png')} 
                style={stylesLeaderBoard.BackGroundImage} 
            >

                <View style={stylesLeaderBoard.ViewUser}>
                    <Image 
                        source={require('../../../images/user.png')}
                        style={stylesLeaderBoard.Image2}
                    />
                    <Text style={stylesLeaderBoard.UserRanking}>{this.state.userRank}</Text>
                    <View style={stylesLeaderBoard.ViewUser2}>
                        <Text style={stylesLeaderBoard.currentUsername}>{this.state.currentUserUsername}</Text>
                        <Text style={stylesLeaderBoard.currentPoint}>{StringsOfLanguages.point}{this.state.currentUserTotalPoint}</Text>
                    </View>
                </View>
                
                <View style={stylesLeaderBoard.ViewFlatlist}>
                    <FlatList
                        style={{margin: 2}}
                        numColumns={3}
                        data={this.state.userArray}
                        renderItem={this._renderMyList}
                        keyExtractor={this._renderMyKeyExtractor}
                    />
                </View>

            </ImageBackground>
        )
    }

    componentDidMount(){
        StatusBar.setHidden(true);
        this.SetCurrentUserPoint();
        this.SetTheSixHighest();
        this.GetCurrentUserRank();
    }

}
