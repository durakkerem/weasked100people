import React, { Component } from 'react'
import { Text, Image, ImageBackground, BackHandler } from 'react-native'
import { AdMobInterstitial } from 'react-native-admob'
import { stylesMatchScreen } from '../../styles/styles'

export default class MatchScreen extends Component {

    constructor(props){
        super(props);
        // For the bot name
        const { uniqueNamesGenerator, names, animals, colors } = require('unique-names-generator');
        this.state = {
            questions: props.navigation.getParam('questions', "NO-QUESTION"),
            randomBotName: uniqueNamesGenerator({ dictionaries: [names, animals, colors], length: 1 }),  
            image: require('../../../images/bot1.png'),
            time: 3
        }
      }

      generateRandomMiliseconds(){
        var RandomNumber = Math.floor(Math.random() * 1000) + 600 ;
        return RandomNumber
      }

      Load_New_Image=()=>{
  
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot2.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot3.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot4.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot2.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot6.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot7.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot8.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot1.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot3.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot4.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot5.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot2.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot7.png')
        }) }, this.generateRandomMiliseconds());
        setTimeout(() => {       this.setState({
            image : require('../../../images/bot8.png')
        }) }, this.generateRandomMiliseconds());
      }

    render() {
        return (
            <ImageBackground 
                source={require('../../../images/background.png')} 
                style={stylesMatchScreen.Container}>

                <Text style={stylesMatchScreen.botName}>{this.state.randomBotName}</Text>

                <Image 
                    source={this.state.image}
                    style={stylesMatchScreen.Images}
                /> 

            </ImageBackground>
        )
    }

    componentDidMount(){
        this.Load_New_Image();

        BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressed);

        // AdMobInterstitial
            AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
            AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
        //

        this.myInterval = setInterval(() => {
            if(this.state.time === 0){
              clearInterval(this.myInterval);
              this.props.navigation.navigate("Game",{
                questions: this.state.questions,
                image: this.state.image,
                randomBotName: this.state.randomBotName
              })
            }
            else{
                this.setState(prevState => ({
                    time: prevState.time - 1
                }));
            }
        }, 1000);

    } //

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressed);
    }
    
    onBackButtonPressed() {
        return true;
    }

}
