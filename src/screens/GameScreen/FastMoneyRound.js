import React, { Component } from 'react'
import {
    Text,
    View,
    TouchableWithoutFeedback,
    Keyboard,
    TextInput,
    TouchableOpacity,
    BackHandler,
    Image,
    ImageBackground,
    StatusBar
} from 'react-native'
import { stylesFastMoney } from '../../styles/styles'
import Toast from 'react-native-toast-message'
import firebase from 'firebase';
import {db} from '../../config';
import StringsOfLanguages from '../../StringsOfLanguages';
import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component'


export default class FastMoneyRound extends Component {

    
    constructor(props){
        super(props);
        this.myTextInput = React.createRef();
        this.state = {
            questions: props.navigation.getParam('questions', "NO-QUESTIONS"),           
            activeQuestion: props.navigation.getParam('questions', "NO-QUESTION")[3],
            currentPoint: props.navigation.getParam('currentPoint', "NO-CURRENTPOINT"),
            image: props.navigation.getParam('image', "NO-IMAGE"),
            currentBotName: props.navigation.getParam('currentBotName', "NO-BOTNAME"),
            BotPoint: props.navigation.getParam('BotPoint', "NO-BOTPOINT"),
            activeQuestionCounter: 4,
            roundDurations: 50,
            temper: 0,
            temper2: 0,
            answers: [],
            totalPoints: [],
            userWord: '',
            tableHead: [' ', StringsOfLanguages.answer, StringsOfLanguages.point2],
            tableTitle: [StringsOfLanguages.question1, StringsOfLanguages.question2, StringsOfLanguages.question3, StringsOfLanguages.question4, StringsOfLanguages.question5],
            tableData: [
              ['', ''],
              ['', ''],
              ['', ''],
              ['', ''],
              ['', '']
            ],
            rnd: [],
            counterr: 0
        }
    }

    Timer(){
        this.myInterval = setInterval(() => {
            if(this.state.roundDurations === 0 || this.state.answers[4] != null){
              clearInterval(this.myInterval);
              this.ShowPoints();             
            }
            else{
              if (this.state.roundDurations === 55 || this.state.roundDurations === 46 || this.state.roundDurations === 37 || this.state.roundDurations === 28 || this.state.roundDurations === 19 || this.state.roundDurations === 8) {
                setTimeout(() => {
                  this.setState(prevState => ({
                    roundDurations: prevState.roundDurations - 1,
                    counterr: this.state.counterr + 1  // Can be changed
                  }));
                }, 500);
                if (this.state.rnd[this.state.counterr] < 3){ // if, bot knew the correct answer then update the bot's point
                this.UpdateBotPoint();
                }
              }
              else{
                this.setState(prevState => ({
                  roundDurations: prevState.roundDurations - 1
                }));
              }
            }
        }, 1000);
    }

    ShowPoints(){
      var temp = this.state.totalPoints
      for(var i=0; i<5; i++){
        if(this.state.totalPoints[i] == null)
        {
          temp[i] = 0;
        }
        this.setState({totalPoints: temp});
      }

      var Total = 0;
      var tempTableData = this.state.tableData;
      for(var i=0; i<5; i++)
      {
        tempTableData[i][1] = this.state.totalPoints[i];
        Total = Total + this.state.totalPoints[i];
      }
      Total = Total + this.state.currentPoint;
      this.setState({tableData: tempTableData}); 

      this.setState({roundDurations: 5},() => {
        this.myInterval = setInterval(() => {
          if(this.state.roundDurations === 0){
            clearInterval(this.myInterval);

            var result = '';
            if(Total > this.state.BotPoint)
            {
              result = StringsOfLanguages.result1;
            }
            else if (Total === this.state.BotPoint)
            {
              result = StringsOfLanguages.result2;
            }
            else
            {
              result = StringsOfLanguages.result3;
            }

            // UPDATE THE USER POİNT FOR THE LEADERBORD
            const { currentPoint } = this.state;

            db.collection("usersCollection").get().then(function(querySnapshot) {
              querySnapshot.forEach(function(doc) {
                if(doc.data().email === firebase.auth().currentUser.email)
                {
                  db.collection('usersCollection').doc(doc.data().username)
                  .update({
                    totalPoint: doc.data().totalPoint + currentPoint 
                  });      
                }
              });
            });
            //----

            this.props.navigation.navigate("Congratulations", { userPoint: Total });

          }
          else{
              this.setState(prevState => ({
                  roundDurations: prevState.roundDurations - 1
              }));
          }
      }, 1000);
      });

    }// end of the ShowPoints Function

    guessTheWord() {
      var FuzzyMatching = require('fuzzy-matching'); // For the fuzzy-matching algorithm
      var fm = new FuzzyMatching([]);
  
      for(var i=0; i<this.state.activeQuestion.answers.length; i++){
        fm.add(this.state.activeQuestion.answers[i].answer);
      }
  
      const { questions, activeQuestionCounter } = this.state;
      var answerCount = activeQuestionCounter - 4;
      var tempWord = this.state.userWord.toUpperCase();
      var tempAnswerCount = this.state.totalPoints;
      
      if(this.state.activeQuestion.answers.some(cred => cred.answer === fm.get(tempWord).value) ){
        this.state.activeQuestion.answers.map(
          (answerObject) => 
            (answerObject.answer === fm.get(tempWord).value) 
            ? tempAnswerCount[this.state.temper2] = answerObject.answerCount 
            : null
        );
      }
      else{
        tempAnswerCount[this.state.temper2] = 0;
      }

      this.setState({
        totalPoints: tempAnswerCount,
        temper2: this.state.temper2 + 1
      });

      var tempAnswers = this.state.answers;
      if(fm.get(tempWord).distance > 0.51){
        tempAnswers.push(fm.get(tempWord).value);
      }
      else{
        tempAnswers.push(tempWord);
      }
      
      this.setState({ answers: tempAnswers }, () => {
        var tempTableData = this.state.tableData;
        tempTableData[answerCount][0] = this.state.answers[answerCount];
        this.setState({
          tableData: tempTableData,
          activeQuestion: questions[activeQuestionCounter],
          activeQuestionCounter: activeQuestionCounter + 1
        });
      });

      this.myTextInput.current.clear();
      if(this.state.temper < 3){
        this.setState({temper: this.state.temper + 1});
      }
    }// enf of the guesstheword function

    chooseTimer(){
      if (this.state.roundDurations > 9) {
        return <Text style={stylesFastMoney.Timer}>{this.state.roundDurations}</Text>
      }
      else
      {
        return <Text style={stylesFastMoney.TimerRed}>{this.state.roundDurations}</Text>
      }
    }

    renderAnimation(){
      if ( (this.state.roundDurations <= 55 && this.state.roundDurations > 54) || (this.state.roundDurations <= 46 && this.state.roundDurations > 45) || (this.state.roundDurations <= 37 && this.state.roundDurations > 36) || (this.state.roundDurations <= 28 && this.state.roundDurations > 27) || (this.state.roundDurations <= 19 && this.state.roundDurations > 18) || (this.state.roundDurations <= 8 && this.state.roundDurations > 7) ) {
        if (this.state.rnd[this.state.counterr] < 3) {
          return <Image source={require('../../../images/tick.gif')} style={stylesFastMoney.Gif} />
        }
        else if (this.state.rnd[this.state.counterr] > 2) {
          return <Image source={require('../../../images/cross.gif')} style={stylesFastMoney.Gif} />
        }
  
      }
      else{
        return <Text style={stylesFastMoney.BotPoint}>{this.state.BotPoint}</Text>
      }
    }

    UpdateBotPoint()
    {
      var temPoint = this.state.BotPoint;
      var length1 = this.state.activeQuestion.answers.length;
      var rndP = Math.floor(Math.random() * length1);
      temPoint = temPoint + this.state.activeQuestion.answers[rndP].answerCount;
      this.setState({BotPoint: temPoint});
    }

    render() {
        const state = this.state;
        return(
          <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
            <ImageBackground source={require('../../../images/quiz_bakcground.png')} style={stylesFastMoney.Container}>

              <View style={stylesFastMoney.ContentTop}>
                <View>{ this.chooseTimer() }</View>
                <View style={stylesFastMoney.BOT}>
                  <Image 
                    source={this.state.image}
                    style={stylesFastMoney.Images}
                  />
                  <View>
                    <Text style={stylesFastMoney.BotName}>{this.state.currentBotName}</Text>
                    {this.renderAnimation()}
                  </View>
                </View>
              </View>
       
              <View style={{top: -41, maxHeight: '33%', minHeight: '33%'}}>
                <Text style={stylesFastMoney.Title}>{StringsOfLanguages.fourthRound}</Text>
                <Text style={stylesFastMoney.SubTitle}>
                  {this.state.activeQuestion.question}
                </Text>
              </View>  

              <View style={stylesFastMoney.tableView}>
                <Table borderStyle={stylesFastMoney.tableBorderStyle}>
                  <TableWrapper style={stylesFastMoney.wrapper}>
                    <Col textStyle={stylesFastMoney.text}/>
                    <Rows data={state.tableData} flexArr={[7]} style={stylesFastMoney.row} textStyle={stylesFastMoney.text}/>
                  </TableWrapper>
                </Table>
              </View>

              <View style={{marginVertical: 5}}>
                <TextInput 
                  style={stylesFastMoney.Input}
                  ref={this.myTextInput} 
                  placeholder={StringsOfLanguages.InputPlaceholder} 
                  onChangeText={(text) => this.setState({userWord: text})}
                  returnKeyType = 'send'                  
                  onSubmitEditing = {() => this.guessTheWord()}
                  placeholderTextColor = '#595a5e'
                />
              </View>


              <View style={{flex: 1, alignItems: 'center'}}>
                  <TouchableOpacity 
                      onPress = {() => this.guessTheWord()}
                  >
                      <ImageBackground 
                          source={require('../../../images/continue_button.png')}
                          style={stylesFastMoney.BackGroundImageContinue}
                      >
                          <Text style={stylesFastMoney.TextContinue}>{StringsOfLanguages.send}</Text>
                      </ImageBackground>
                  </TouchableOpacity>
              </View>

            </ImageBackground>
          </TouchableWithoutFeedback>
          
        )//end of the return
    }// end of the render function

    componentDidMount(){  
      StatusBar.setHidden(true);
      this.Timer();  // Round one - Timer start
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressed);

      var rndX;
      var rndLocal = this.state.rnd;
      for(var i = 0 ; i<18; i++){
        rndX = Math.floor(Math.random() * 10) + 1;
        rndLocal.push(rndX);
      }
      this.setState({
        rnd: rndLocal
      });
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressed);
    }

    onBackButtonPressed() {
      return true;
    }

} // end of the class