import React, { Component } from 'react'
import { Text, View, Image, ImageBackground, TouchableOpacity, StatusBar } from 'react-native'
import StringsOfLanguages from '../../StringsOfLanguages';
import { AdMobInterstitial } from 'react-native-admob'
import { stylesCongrat } from '../../styles/styles'


export default class CongratScreen extends Component {

    constructor(props){
        super(props);
        this.state = {
            userPoint: this.props.navigation.getParam('userPoint', "NO-USERPOINT")
        }
    }

    ShowAdvertisementAndGo()
    {
      AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
      AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd()).then(() => this.props.navigation.navigate("Login"));
    }

    render() {
        return (
            <ImageBackground source={require('../../../images/Congrats.png')} style={stylesCongrat.BackGroundImage}>
                <View style={stylesCongrat.mainView}>
                    <View>
                        <Image 
                            source={require('../../../images/user.png')}
                            style={stylesCongrat.Icon}
                        />
                    </View>

                    <View style={{top: -21}}>
                        <Text style={stylesCongrat.TextStyle}>{StringsOfLanguages.congrats}</Text>
                        <Text style={stylesCongrat.TextStyle2}>{StringsOfLanguages.earned1}{this.state.userPoint}{StringsOfLanguages.earned2}</Text>     
                    </View>

                    <View>
                        <TouchableOpacity 
                            onPress = {() => this.ShowAdvertisementAndGo()}
                        >
                            <ImageBackground 
                                source={require('../../../images/continue_button.png')}
                                style={stylesCongrat.BackGroundImageContinue}
                            >
                                <Text style={stylesCongrat.TextContinue}>{StringsOfLanguages.congratButtonText}</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        )
    }
    componentDidMount() {
        StatusBar.setHidden(true);
    }
}
