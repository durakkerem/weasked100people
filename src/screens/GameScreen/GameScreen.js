import React, { Component } from 'react'
import {
  View, 
  Text,
  TouchableWithoutFeedback, 
  Keyboard, 
  Alert, 
  TextInput,
  TouchableOpacity, 
  BackHandler, 
  Image,
  ImageBackground,
  FlatList,
  StatusBar
} from 'react-native'
import Toast from 'react-native-toast-message'
import firebase from 'firebase';
import {db} from '../../config';
import { AdMobInterstitial } from 'react-native-admob'
import StringsOfLanguages from '../../StringsOfLanguages';
import { stylesGame } from '../../styles/styles'


export default class GameScreen extends Component{
  constructor(props){
    super(props);
    this.myTextInput = React.createRef();
    this.state = {
      questions: props.navigation.getParam('questions', "NO-QUESTION"),           // props-parameter
      activeQuestion: props.navigation.getParam('questions', "NO-QUESTION")[0],  // props-parameter
      image: props.navigation.getParam('image', "NO-IMAGE"),                    // props-parameter  --- Navigate to FastMoney !
      randomBotName: props.navigation.getParam('randomBotName', "NO-randomBotName"),// props-parameter --- Random BOT NAME
      guessedWords: [],                                                         // Guessed word list of this round
      userWord: '',                                                            // User guess
      wrongGuessCount: 0,                                                     // The number of rights before the score starts to decrease
      currentPoint:0,                                                         // Round Point of this round
      roundBasedPoints: [],                                                   // Total Round Point's
      roundDurations: 60,                                                     // Timer
      roundCount: 1,                                                          // First / Second / Third / Fourth ROUND
      fiveSecTimeout:false,                                                   // between five seconds control, after finish round and  before popup screen 
      activeQuestionCounter: 1,
      currentBotName: '',           // Navigate to FastMoney !
      BotPoint: 0,                  // Navigate to FastMoney !
      rnd: [],
      counterr: 0,
      willUpdatedPoint: 0,
      guessTimes: []
    }
  }

  guessTheWord() {
    var FuzzyMatching = require('fuzzy-matching'); // For the fuzzy-matching algorithm
    var fm = new FuzzyMatching([]);

    for(var i=0; i<this.state.activeQuestion.answers.length; i++){
      fm.add(this.state.activeQuestion.answers[i].answer);
    }

    let tempWord = this.state.userWord.toUpperCase();  {/* Convert the incoming word to its uppercase version and transfer it to tempWord. */}


    if (this.state.guessedWords.some(cred => cred.answer === fm.get(tempWord).value) && tempWord != "") {
      Toast.show({
        type: 'info',
        text1: StringsOfLanguages.alert1
      });
      this.myTextInput.current.clear();
    }

    else{
      if( fm.get(tempWord).distance > 0.51 ){  {/* if user knew correct answer OR if the word the user typed is 51% similar to one of the answers */}

        let tempAnswerCount = 0;
        let tempPointContainer = this.state.currentPoint;  // Point
        
        this.state.activeQuestion.answers.map(  
          (answerObject) => (answerObject.answer === fm.get(tempWord).value) ? tempAnswerCount=answerObject.answerCount : null );       
        
        this.state.guessedWords.map((word) => word.answer === fm.get(tempWord).value ? tempAnswerCount = 0: null);
      
        if(this.state.roundCount === 1){
          tempPointContainer += tempAnswerCount;
        }
        if(this.state.roundCount === 2){
          tempPointContainer += tempAnswerCount * 2;
        }
        if(this.state.roundCount === 3){
          tempPointContainer += tempAnswerCount * 3;
        }

        let tempArray = this.state.guessedWords;
        tempArray.push({answer: fm.get(tempWord).value});
        this.setState({guessedWords: tempArray});

        this.setState({currentPoint: tempPointContainer});


        Toast.show({
          type: 'success',
          text1: StringsOfLanguages.alert2
        });
        this.myTextInput.current.clear(); {/* Clear the TextInput's text */}
      }

      else{   /* if user didn't know any correct answer */      

          let tempCounter = this.state.wrongGuessCount
          tempCounter++
          this.setState({wrongGuessCount: tempCounter})

          if(tempCounter > 3) // if there is no right
            {
              if (this.state.roundCount === 1) { // for the round one
                this.myTextInput.current.clear(); {/* Clear the TextInput's text */}
                let tempCount = this.state.currentPoint;
                tempCount = tempCount - 5 ;
                this.setState({currentPoint: tempCount}); // for the point counter
                let three = 3;
                this.setState({wrongGuessCount: three});  // for the life counter
                Toast.show({
                  type: 'error',
                  text1: StringsOfLanguages.alert3
                });
              }
              else if (this.state.roundCount === 2) { // for the round two
                this.myTextInput.current.clear(); {/* Clear the TextInput's text */}
                let tempCount = this.state.currentPoint;
                tempCount = tempCount - 10 ;
                this.setState({currentPoint: tempCount}); // for the point counter
                let three = 3;
                this.setState({wrongGuessCount: three});  // for the life counter
                Toast.show({
                  type: 'error',
                  text1: StringsOfLanguages.alert3
                });
              }
              else if (this.state.roundCount === 3) { // for the round three
                this.myTextInput.current.clear(); {/* Clear the TextInput's text */}
                let tempCount = this.state.currentPoint;
                tempCount = tempCount - 15 ;
                this.setState({currentPoint: tempCount}); // for the point counter
                let three = 3;
                this.setState({wrongGuessCount: three});  // for the life counter
                Toast.show({
                  type: 'error',
                  text1: StringsOfLanguages.alert3
                });
              }
            }
          else
            {
              this.myTextInput.current.clear(); {/* Clear the TextInput's text */}
              Toast.show({
                type: 'error',
                text1: StringsOfLanguages.alert3
              });
            }
      }
    }
    
  }

  printTitle(){
    const {roundCount} = this.state;
    switch (roundCount) {
      case 1:
        return <Text style={stylesGame.Title}>{StringsOfLanguages.firstRound}</Text>
        break;
      case 2:
        return <Text style={stylesGame.Title}>{StringsOfLanguages.secondRound}</Text>
        break;
      case 3:
        return <Text style={stylesGame.Title}>{StringsOfLanguages.thirdRound}</Text>
        break;
      default:
        return <Text style={stylesGame.Title}>NaN</Text>
        break;
    }
  }

  includeTotalPoint(){
    let totalPoint = 0;
    for(var i = 0; i<4; i++){
      totalPoint += this.state.roundBasedPoints[i];
    }
    return totalPoint;
  }

  beforeNextRound(){
    //  Set the variables of the state
    //  Change the activeQuestion
    //  Start the next round
    const {roundCount} = this.state;
    const {questions} = this.state;
    const {activeQuestionCounter} = this.state;
    const {currentPoint} = this.state;
    this.setState({activeQuestionCounter: activeQuestionCounter + 1});
    var tempPointsList = this.state.roundBasedPoints;
    tempPointsList.push(currentPoint);

      if(roundCount === 3){   // FAST MONEY ROUND - ROUND FOUR !!
        this.setState({
          roundCount: roundCount + 1,
          roundBasedPoints: tempPointsList,
          activeQuestion: questions[activeQuestionCounter]    
        });
      }
      else{
        this.setState({
          roundCount: roundCount + 1,
          roundDurations: 60,
          roundBasedPoints: tempPointsList,
          activeQuestion: questions[activeQuestionCounter],
          currentBotName: this.state.randomBotName,
          guessedWords: [],
          wrongGuessCount: []
        });
      }
      this.Timer();
      this.printTitle();

  } // finish function
  

  Timer(){
    // Timer
    this.myInterval = setInterval(() => {     // 60 seconds start
      if(this.state.roundDurations === 0){
        clearInterval(this.myInterval);
        // END ROUND !

        // Show the question answers of user didnt know
        // Show a pop up window
        // Show round score
        // Create a button and this button start the second round
        var tempArray = new Array();                                   // unlocked answers after finish round
        let length = this.state.activeQuestion.answers.length;
        for(var i=0;i<length;i++)
        {
          var tempWord = this.state.activeQuestion.answers[i].answer;
          tempArray.push({answer: tempWord});
        }
        this.setState({guessedWords: tempArray});
//--------------------------------------------------------------------------------------------------------------------------
        if(this.state.fiveSecTimeout === false){
          this.setState({
            roundDurations: 5
          });
          this.myInterval = setInterval(() => {
            if(this.state.roundDurations === 0){
              if(this.state.roundCount === 3){
                clearInterval(this.myInterval);

                if(this.state.BotPoint > this.state.currentPoint || ( this.state.BotPoint === 0 && this.state.currentPoint === 0 ))  // VICTORY AGAINST BOT
                {
                  // UPDATE THE USER POİNT FOR THE LEADERBORD
                  const { currentPoint } = this.state;

                  db.collection("usersCollection").get().then(function(querySnapshot) {
                    querySnapshot.forEach(function(doc) {
                      if(doc.data().email === firebase.auth().currentUser.email)
                      {
                        db.collection('usersCollection').doc(doc.data().username)
                        .update({
                          totalPoint: doc.data().totalPoint + currentPoint 
                        });      
                      }
                    });
                  });
                  // UPDATE THE USER POİNT FOR THE LEADERBORD
                  

                  Alert.alert( // LOST
                    StringsOfLanguages.alert4,
                    StringsOfLanguages.alert5 + "\n" + StringsOfLanguages.alert6 + "\n" + StringsOfLanguages.alert7 +"\n\n"+
                     StringsOfLanguages.alert8 + this.state.currentPoint +"\n"+ StringsOfLanguages.alert9 + this.state.BotPoint,
                    [     
                      {
                        text: StringsOfLanguages.alert10, 
                        onPress: () => this.ShowAdvertisementAndGo(),
                        style:'default'
                      },
                    ],   
                    { cancelable: false }, 
                  );
                }
                else // WIN
                {
                  Alert.alert(
                    StringsOfLanguages.alert11,
                    StringsOfLanguages.alert12+"\n"+StringsOfLanguages.alert13 +"\n\n"+ StringsOfLanguages.alert8 
                    + this.state.currentPoint +"\n"+ StringsOfLanguages.alert9 + this.state.BotPoint,
                    [     
                      {
                        text: StringsOfLanguages.alert14, 
                        onPress: () => this.props.navigation.navigate("FastMoney",{
                          questions: this.state.questions,
                          currentPoint: this.state.currentPoint,
                          image: this.state.image,
                          currentBotName: this.state.currentBotName,
                          BotPoint: this.state.BotPoint 
                        }),
                        style:'default'
                      },        
                    ],   
                    { cancelable: false }, 
                  );
                } // end of the else
              } // end of the if-roundCount === 3

              else{
                clearInterval(this.myInterval);
                Alert.alert(
                  StringsOfLanguages.alert15,
                  StringsOfLanguages.alert16+"\n"+StringsOfLanguages.alert17 +"\n\n"+
                   StringsOfLanguages.alert8 + this.state.currentPoint +"\n"+ StringsOfLanguages.alert9 + this.state.BotPoint,
                  [     
                    {
                      text: StringsOfLanguages.alert18, 
                      onPress: () => this.beforeNextRound(),
                      style:'default'
                    },        
                  ],   
                  { cancelable: false }, 
                );
              }
            }
            else{
              this.setState(prevState => ({
                roundDurations: prevState.roundDurations - 1
              }));
            }
          }, 1000)
        } // end of the if block two

      } // enf of the if block one 
      else{ // if roundDurations doesnt zero.
        // 55-46-37-28-19-8

        // Generate random guess 
        switch (this.state.guessTimes.length) {
          case 4:
            if (this.state.roundDurations === this.state.guessTimes[0] ||
                this.state.roundDurations === this.state.guessTimes[1] ||
                this.state.roundDurations === this.state.guessTimes[2] ||
                this.state.roundDurations === this.state.guessTimes[3] ) {

              setTimeout(() => {
                this.setState(prevState => ({
                  roundDurations: prevState.roundDurations - 1,
                  counterr: this.state.counterr + 1  
                }));
              }, 500);
              if (this.state.rnd[this.state.counterr] < 4){ // if, bot knew the correct answer then update the bot's point
              this.UpdateBotPoint();
              }
              
            }
    
            else {
              this.setState(prevState => ({
                roundDurations: prevState.roundDurations - 1
              }));
            }
            break;

          case 5:
            if (this.state.roundDurations === this.state.guessTimes[0] ||
                this.state.roundDurations === this.state.guessTimes[1] ||
                this.state.roundDurations === this.state.guessTimes[2] ||
                this.state.roundDurations === this.state.guessTimes[3] ||
                this.state.roundDurations === this.state.guessTimes[4] ) {

              setTimeout(() => {
                this.setState(prevState => ({
                  roundDurations: prevState.roundDurations - 1,
                  counterr: this.state.counterr + 1  
                }));
              }, 500);
              if (this.state.rnd[this.state.counterr] < 4){ // if, bot knew the correct answer then update the bot's point
              this.UpdateBotPoint();
              }
              
            }
    
            else {
              this.setState(prevState => ({
                roundDurations: prevState.roundDurations - 1
              }));
            }
            break;

          case 6:
            if (this.state.roundDurations === this.state.guessTimes[0] ||
                this.state.roundDurations === this.state.guessTimes[1] ||
                this.state.roundDurations === this.state.guessTimes[2] ||
                this.state.roundDurations === this.state.guessTimes[3] ||
                this.state.roundDurations === this.state.guessTimes[4] ||
                this.state.roundDurations === this.state.guessTimes[5] ) {

              setTimeout(() => {
                this.setState(prevState => ({
                  roundDurations: prevState.roundDurations - 1,
                  counterr: this.state.counterr + 1  
                }));
              }, 500);
              if (this.state.rnd[this.state.counterr] < 4){ // if, bot knew the correct answer then update the bot's point
              this.UpdateBotPoint();
              }
              
            }
    
            else {
              this.setState(prevState => ({
                roundDurations: prevState.roundDurations - 1
              }));
            }
            break;
        
          default:
            null;
            break;
        }

        this.setState({fiveSecTimeout: false});
      }
    }, 1000)
  }

  ShowAdvertisementAndGo()
  {
    AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
    AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd()).then(() => this.props.navigation.navigate("Login"));
  }

  Skip(){
    this.setState({roundDurations: 0});
  }
  
  chooseTimer(){
    if (this.state.roundDurations > 9) {
      return <Text style={stylesGame.Timer}>{this.state.roundDurations}</Text>
    }
    else
    {
      return <Text style={stylesGame.TimerRed}>{this.state.roundDurations}</Text>
    }
  }
  
  renderAnimation(){
    switch (this.state.guessTimes.length) {
      case 4:
        if (  (this.state.roundDurations <= this.state.guessTimes[0] && this.state.roundDurations > (this.state.guessTimes[0] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[1] && this.state.roundDurations > (this.state.guessTimes[1] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[2] && this.state.roundDurations > (this.state.guessTimes[2] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[3] && this.state.roundDurations > (this.state.guessTimes[3] - 1)) ) {
          if (this.state.rnd[this.state.counterr] < 4) {
            return <Image source={require('../../../images/tick.gif')} style={stylesGame.Gif} />
          }
          else if (this.state.rnd[this.state.counterr] > 3) {
            return <Image source={require('../../../images/cross.gif')} style={stylesGame.Gif} />
          }

        }
        else{
          return <Text style={stylesGame.BotPoint}>{this.state.BotPoint}</Text>
        }
        break;

      case 5:
        if (  (this.state.roundDurations <= this.state.guessTimes[0] && this.state.roundDurations > (this.state.guessTimes[0] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[1] && this.state.roundDurations > (this.state.guessTimes[1] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[2] && this.state.roundDurations > (this.state.guessTimes[2] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[3] && this.state.roundDurations > (this.state.guessTimes[3] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[4] && this.state.roundDurations > (this.state.guessTimes[4] - 1)) ) {
          if (this.state.rnd[this.state.counterr] < 4) {
            return <Image source={require('../../../images/tick.gif')} style={stylesGame.Gif} />
          }
          else if (this.state.rnd[this.state.counterr] > 3) {
            return <Image source={require('../../../images/cross.gif')} style={stylesGame.Gif} />
          }

        }
        else{
          return <Text style={stylesGame.BotPoint}>{this.state.BotPoint}</Text>
        }
        break;

      case 6:
        if (  (this.state.roundDurations <= this.state.guessTimes[0] && this.state.roundDurations > (this.state.guessTimes[0] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[1] && this.state.roundDurations > (this.state.guessTimes[1] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[2] && this.state.roundDurations > (this.state.guessTimes[2] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[3] && this.state.roundDurations > (this.state.guessTimes[3] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[4] && this.state.roundDurations > (this.state.guessTimes[4] - 1)) ||
              (this.state.roundDurations <= this.state.guessTimes[5] && this.state.roundDurations > (this.state.guessTimes[5] - 1)) ) {
          if (this.state.rnd[this.state.counterr] < 4) {
            return <Image source={require('../../../images/tick.gif')} style={stylesGame.Gif} />
          }
          else if (this.state.rnd[this.state.counterr] > 3) {
            return <Image source={require('../../../images/cross.gif')} style={stylesGame.Gif} />
          }
    
        }
        else{
          return <Text style={stylesGame.BotPoint}>{this.state.BotPoint}</Text>
        }
        break;
    
      default:
        null;
        break;
    }
  }

  UpdateBotPoint()
  {
    var temPoint = this.state.BotPoint;

    var length1 = this.state.activeQuestion.answers.length;
    var rndP = Math.floor(Math.random() * length1);

    if(this.state.roundCount === 1)
    {
      temPoint = temPoint + this.state.activeQuestion.answers[rndP].answerCount;
    }
    else if(this.state.roundCount === 2)
    {
      temPoint = temPoint + (this.state.activeQuestion.answers[rndP].answerCount * 2);
    }
    else if(this.state.roundCount === 3)
    {
      temPoint = temPoint + (this.state.activeQuestion.answers[rndP].answerCount * 3);
    }
    this.setState({BotPoint: temPoint});
  }


  _renderMyList = ({item})=>(
    this.state.guessedWords.some(cred => cred.answer === item.answer)
    ) ? <Text style={stylesGame.Answer}> {item.answer}  -  {item.answerCount} </Text> 
      : <Text style={stylesGame.Unknown}> ? </Text> 

  _renderMyKeyExtractor=(item, index) => index.toString();

  render(){
    return(
      <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}}>
        <ImageBackground source={require('../../../images/quiz_bakcground.png')} style={stylesGame.Container}>

          <View style={stylesGame.ContentTop}>
            <View>{ this.chooseTimer() }</View>
            <View style={stylesGame.BOT}>
              <Image 
                source={this.state.image}
                style={stylesGame.Images}
              />
              <View>
                <Text style={stylesGame.BotName}>{this.state.randomBotName}</Text>
                {this.renderAnimation()}
              </View>
            </View>
          </View>


          <View style={{top: -42, maxHeight: '32%', minHeight: '32%'}}>
            { this.printTitle() }
            <Text style={stylesGame.SubTitle}>
              {this.state.activeQuestion.question}
            </Text>
          </View> 
            

          <View style={stylesGame.AnswerContainer}>
            <FlatList
              style={{marginTop: 0}}
              numColumns={2}
              data={this.state.activeQuestion.answers}
              renderItem={this._renderMyList}
              keyExtractor={this._renderMyKeyExtractor}
            /> 
          </View>


          <Toast ref={(ref) => Toast.setRef(ref)} />
          <View>
            <TextInput 
              style = {stylesGame.Input}
              ref = {this.myTextInput} 
              placeholder = {StringsOfLanguages.InputPlaceholder} 
              onChangeText = {(text) => this.setState({userWord: text})}
              returnKeyType = 'send'                  
              onSubmitEditing = {() => this.guessTheWord()}
              placeholderTextColor = '#595a5e'
            />
          </View>


          <View style={{flex: 1, alignItems: 'center'}}>
              <TouchableOpacity 
                  onPress = {() => this.guessTheWord()}
              >
                  <ImageBackground 
                      source={require('../../../images/continue_button.png')}
                      style={stylesGame.BackGroundImageContinue}
                  >
                      <Text style={stylesGame.TextContinue}>{StringsOfLanguages.send}</Text>
                  </ImageBackground>
              </TouchableOpacity>
          </View>


          <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end'}}>
            <TouchableOpacity 
              onPress = {() => this.Skip()}
            >
              <Image 
                source={require('../../../images/arrow.png')}
                style={stylesGame.Arrow}
              />   
            </TouchableOpacity>
          </View>

        </ImageBackground>
      </TouchableWithoutFeedback>
    )
  }

  componentDidMount(){
    this.Timer(); // Round one - Timer start

    //#region Disable status bar and android back button
    StatusBar.setHidden(true);
    BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressed);
    //#endregion

    //#region True, false info for a every question - Step 1
    var rndX;
    var rndLocal = this.state.rnd;
    for(var i = 0 ; i<18; i++){
      rndX = Math.floor(Math.random() * 9) + 1;
      rndLocal.push(rndX);
    }
    this.setState({
      rnd: rndLocal
    });
    //#endregion

    //#region BOT GUESS LENGTH AND TİME
    var guessLength = Math.floor(Math.random() * 3) + 4;
    var guessTimes = this.state.guessTimes;
    for(var i=0; i<guessLength; i++){
      var times = Math.floor(Math.random() * 49) + 7;
      guessTimes.push(times);
    }
    guessTimes.sort(function(a, b){return b-a});
    console.log(guessTimes.toString(),"  Length: ",guessTimes.length);
    
    for(var i=0; i<this.state.guessTimes.length; i++)
    {
      if(this.state.guessTimes[i] - this.state.guessTimes[i-1] < 6 && this.state.guessTimes[i] - this.state.guessTimes[i-1] > -6)
      {
        guessTimes[i] = guessTimes[i] - 5;
      }
      guessTimes.sort(function(a, b){return b-a});
    }
    console.log(guessTimes.toString());
    this.setState({guessTimes: guessTimes});
    //#endregion
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressed);
  }

  onBackButtonPressed() {
    return true;
  }
}       


       

