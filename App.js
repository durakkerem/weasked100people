import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import LoginScreen from './src/screens/LoginScreen/LoginScreen'
import GameScreen from './src/screens/GameScreen/GameScreen'
import SignupScreen from './src/screens/SignupScreen/SignupScreen'
import FastMoneyRound from './src/screens/GameScreen/FastMoneyRound'
import MatchScreen from './src/screens/GameScreen/MatchScreen'
import SplashScreen from './src/screens/SplashScreen/SplashScreen'
import ForgotPassword from './src/screens/ForgotPassword/ForgotPassword'
import LeaderBoard from './src/screens/LeaderBoard/LeaderBoard'
import Congratulations from './src/screens/GameScreen/CongratScreen'

const navigator = createStackNavigator(
  {
    Game: {
      screen: GameScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    Signup: {
      screen: SignupScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    FastMoney: {
      screen: FastMoneyRound,
      navigationOptions: {
        headerShown: false
      }
    },
    Match: {
      screen: MatchScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    Splash: {
      screen: SplashScreen,
      navigationOptions: {
        headerShown: false
      }
    },
    ForgotPW: {
      screen: ForgotPassword,
      navigationOptions: {
        headerShown: false
      }
    },
    LeaderBoard: {
      screen: LeaderBoard,
      navigationOptions: {
        headerShown: false
      }
    },
    Congratulations: {
      screen: Congratulations,
      navigationOptions: {
        headerShown: false
      }
    }
  },
  {
    initialRouteName: 'Splash', 
    defaultNavigationOptions: {
    title: 'WeAsked100People'
    },
    
  }
);
export default createAppContainer(navigator);
