# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 01:38:28 2020

@author: csdur
"""

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import json, codecs, time, json, ast, copy
import pandas as pd

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate("asked100people-firebase-adminsdk-vzdzm-1e080adb40.json")
firebase_admin.initialize_app(cred)
db = firestore.client()


collection = db.collection(u'questionsEng')


# install adblocker and fire up chrome
chrome_options = Options()
chrome_options.add_argument("load-extension=C:/Users/csdur/AppData/Local/Google/Chrome/User Data/Default/Extensions/gighmmpiobklfepjocnamgkkbiglidom/4.24.1_0")
driver = webdriver.Chrome(executable_path="C:/Users/csdur/Desktop/chromedriver.exe",chrome_options=chrome_options)
driver.create_options()
driver.get("http://www.google.com")


driver.get("https://www.familyfeudinfo.com/sitemap.php")

allquestions = driver.find_element_by_class_name("content").find_elements_by_tag_name("a")
all_urls = []
for item in allquestions:
    if(len(all_urls)%100==0):
        print(len(all_urls))
    all_urls.append(item.get_attribute("href"))


def parse_question_data(url):
    driver.get(url)
    result_html = driver.find_element_by_class_name("content")
    result = {}
    result["question"] = result_html.find_element_by_tag_name("h1").text.replace('’', '\'')
    
    answers_html = result_html.find_elements_by_tag_name("tr")
    result["answers"] = []
    for item in answers_html:
        
        if(int(item.find_elements_by_tag_name("td")[1].text) > 0):
            result["answers"].append({
                "answerCount": int(item.find_elements_by_tag_name("td")[1].text), 
                "answer": item.find_element_by_tag_name("th").text.upper().replace('’', '\''), })
    
    return result

def insertToFirestore(document, index): 
    doc_ref = collection.document(str(index))
    doc_ref.set(document)
    
    
# Start collecting the question data
familyfeudquestions = []
for ind, url in enumerate(all_urls):
    if(ind<92):
        continue
    print("{} | {}".format(ind, url))
    
    try:
        result = parse_question_data(url)
        if(ind%10==0):
            print(result)
        familyfeudquestions.append(result)
        time.sleep(0.5)
    except Exception as e:
        print("fail: {}".format(str(e)))
                                
        
                                
df = pd.DataFrame(familyfeudquestions)
df.to_json("familyfeudquestions.json",orient='records')
df.to_excel("familyfeudquestions.xlsx")


dataFrame = pd.read_excel("familyfeudquestions.xlsx")




documentIndex = 0;
for item, value in dataFrame.iterrows():
    if(item%100==0):
        print(item)
    isValid = True # if answer data is unrealistic or wrong, this document will not be inserted
    answers = ast.literal_eval(value["answers"])
    clean_answers = copy.deepcopy(answers)
    for ind, answer in enumerate(answers):
        if(answer["answer"] == "SEND US YOUR ANSWERS!"):
            clean_answers.remove(answer)
        if(answer["answerCount"] > 100):
            isValid = False
        

        
    if(len(clean_answers) < 3):
        isValid = False
    if(isValid):
       
        document = {"question": value["question"], "answers": clean_answers}
        insertToFirestore(document, documentIndex)
        documentIndex+=1
        
        
        